//A program to demonstrate dynamic loading of classes, i.e. loading of classes at runtime, and their introspection using reflection. 

import java.lang.reflect.*;
 
// class whose name will be passed at runtime.

class Spring
{
  String s="From Spring Class.";

 Spring()
{
  System.out.println("no-parameter constructor.");

}

Spring(String s)
{
  System.out.println("1-parameter constructor, value="+s);

}

  public void display()
{
System.out.println(s);

}
public void display(String s1)
{
  System.out.println(s1);
}

}
class Hibernate
{
 int number;
Hibernate()
{

}

Hibernate(int number)
{
 this.number=number;
}
 void show()
{
 System.out.println();
}
void show(int a)
{
System.out.println(++a);
}
}


//class containing the main method.
class DynamicObject
{
   
   //main method.
   public static void main(String args[]) throws Exception
{
  Class[] noparams = {};
  Class[] paramString = new Class[1];	
  paramString[0] = String.class;

Class cls=null;
  try
{
    //creating class object.
     cls=Class.forName(args[0]); 
}
catch(ClassNotFoundException e)
{
  System.out.println("Entered wrong class.");
}

catch(ArrayIndexOutOfBoundsException e)
{
  System.out.println("ArrayIndexOutOfBoundsException");
}
Class cls1=null;
try
{
  cls1=Class.forName(args[1]);
}
catch(ClassNotFoundException e)
{
  System.out.println("Entered wrong class.");
}
catch(ArrayIndexOutOfBoundsException e)
{
  System.out.println("ArrayIndexOutOfBoundsException");
}
 
Object j=null;
try
{   
//creating an instance of object of class given at runtime.
    j=cls.newInstance();
   
}
catch(InstantiationException e)
{
  System.out.println("Cannot instantiate class.");
}

Object i=null;
try
{   
//creating an instance of object of class given at runtime.
     i=cls1.newInstance();
   
}
catch(InstantiationException e)
{
  System.out.println("Cannot instantiate class.");
}



    System.out.println("Objects created.");
    System.out.println("");

   //Constructors.
    Constructor con[] = cls.getDeclaredConstructors(); 
          
        System.out.println("Declared Constructors present:");
          
        // iterating through all constructors 
        for (Constructor constructor : con)  
        { 
            System.out.println(constructor); 
        } 
    
    Constructor con1 = cls.getDeclaredConstructor();
    System.out.println("Constructor 1:"+con1);
     Object object = con1.newInstance();
      System.out.println("Object: " + object.toString());
     
    Constructor con2 = cls.getDeclaredConstructor(String.class);
    System.out.println("Constructor 2:"+con2);
   Object object1 = con2.newInstance("Value passed through constructor.");
      System.out.println("Object: " + object1.toString());

    System.out.println("");

   //Fields.
   Field f[] = cls.getDeclaredFields();
   System.out.println("Declared fields present:");
    // iterating through all fields.
   for (Field field : f)  
        { 
            System.out.println(field); 
        }    

  System.out.println("");

  //Methods.
   Method[] m=cls.getDeclaredMethods();
   System.out.println("Declared methods in the class:");
   for (Method Method : m)  
        { 
            System.out.println(Method); 
        } 

  System.out.println("");


   Method meth1=cls.getDeclaredMethod("display",noparams);
   System.out.println("Method1 of class Simple: "+meth1);
   meth1.invoke(j,null);
   System.out.println("");

   Method meth=cls.getDeclaredMethod("display",paramString);
   System.out.println("Method2 of class Simple: "+meth);
   meth.invoke(j,new String("Value passed through method2."));
   

}


}